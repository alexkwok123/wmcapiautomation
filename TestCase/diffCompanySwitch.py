import glob
import json
import os
from pathlib import Path

import requests

user_profile = os.environ['USERPROFILE']
user_folder_path = user_profile.replace('\\','\\')

path, dirs, files = next(os.walk(user_folder_path+'\\Documents\\API\\INPUT\\\diffCompanySwitch\\body\\'))
file_count_diffComSwitchBody = len(files)

Jar_cmd = os.popen('java -jar '+ user_folder_path+'\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/accounts/differentCompanySwitching').read()
Default = open(user_folder_path+"\\Documents\\API\\INPUT\\default_diffCompanySwitch.json", 'w')
Default.write('{\n'+Jar_cmd+'}')
Default.close()

with open(user_folder_path+"\\Documents\\API\\INPUT\\default_diffCompanySwitch.json", 'r') as outfile_sub:
    json_de_diffComSwitch = outfile_sub.read()
    requests_de_diffComSwitch = json.loads(json_de_diffComSwitch)
    #print(requests_de)

if file_count_diffComSwitchBody == 0:
    print("there is missing different company switch body")
else:
    for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\diffCompanySwitch\\body\\*.json'):
        with open(file, 'r') as f:
            diffComSwitch_body = json.load(f)
            diffComSwitch_body = str(diffComSwitch_body)
            diffComSwitch_body = diffComSwitch_body.replace('\'', '\"')
            requests_body_diffComSwitch = json.loads(diffComSwitch_body)
            Path(file).stem
            # for i in range(len(bodyData)):
            # temp = tmp.replace("'", '"')
            # tweets[0]["accountid"]
            # temp = json.dumps(tmp, indent=4)

            url = "https://localhost:8181" + requests_de_diffComSwitch["url"]
            apikey = requests_de_diffComSwitch["apikey"]
            signature = requests_de_diffComSwitch["signature"]
            timestamp = requests_de_diffComSwitch["timestamp"]

            diffComSwitch_body = diffComSwitch_body

            payload = diffComSwitch_body

            headers = {
                'apikey': apikey,
                'signature': signature,
                'timestamp': timestamp,
                'Content-Type': "application/json"
            }

            response_diffComSwitch = requests.request("POST", url, headers=headers, data=payload, verify=False)
            responseObj_diffComSwitch = json.loads(response_diffComSwitch.text)

            with open(user_folder_path + "\\Documents\\API\\OUTPUT\\diffComSwitch_output_" + Path(file).stem + ".json",
                        'w') as outfile:
                json.dump(responseObj_diffComSwitch, outfile, indent=4)
                outfile.close()
                print(responseObj_diffComSwitch)
print("diff Company Switch is completed")