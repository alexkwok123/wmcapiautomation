import glob
import json
import os
from pathlib import Path

import requests

user_profile = os.environ['USERPROFILE']
user_folder_path = user_profile.replace('\\','\\')

Jar_cmd = os.popen('java -jar '+ user_folder_path+'\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/accounts/pendingTransaction/csv').read()
Default = open(user_folder_path+"\\Documents\\API\\INPUT\\default_pendingTrans.json", 'w')
Default.write('{\n'+Jar_cmd+'}')
Default.close()

with open(user_folder_path+"\\Documents\\API\\INPUT\\default_pendingTrans.json", 'r') as outfile_pend:
    json_de_pend = outfile_pend.read()
    requests_de_pend = json.loads(json_de_pend)

path, dirs, files = next(os.walk(user_folder_path+'\\Documents\\API\\INPUT\\\pendingTrans\\body\\'))
file_count_pendbody = len(files)
path, dirs, files = next(os.walk(user_folder_path+'\\Documents\\API\\INPUT\\\pendingTrans\\header\\'))
file_count_pendheader = len(files)
if file_count_pendheader == 0:
    print("there is missing header")
else:
    for file in glob.glob(user_folder_path+'\\Documents\\API\\INPUT\\\pendingTrans\\header\\*.json'):
        with open(file, 'r') as f:
            pend_header = json.load(f)
            pend_header = str(pend_header)
            pend_header = pend_header.replace('\'','\"')
            requests_header_pend = json.loads(pend_header)
            Path(file).stem
            # for i in range(len(bodyData)):
            # temp = tmp.replace("'", '"')
            # tweets[0]["accountid"]
            # temp = json.dumps(tmp, indent=4)

            if file_count_pendbody !=0:
                for file in glob.glob(user_folder_path+'\\Documents\\API\\INPUT\\\pendingTrans\\header\\*.json'):
                    with open(file, 'r') as f:
                        pend_body = json.load(f)
                        pend_body = str(pend_body)
                        pend_body = pend_body.replace('\'', '\"')
                        requests_body_pend = json.loads(pend_body)
            else:
                url = "https://localhost:8181" + requests_de_pend["url"]
                apikey = requests_de_pend["apikey"]
                signature = requests_de_pend["signature"]
                timestamp = requests_de_pend["timestamp"]
                startCreateDateTime = requests_header_pend["startCreateDateTime"]
                endCreateDateTime = requests_header_pend["endCreateDateTime"]
                pend_body = {}

                payload = pend_body

                headers = {
                    'apikey': apikey,
                    'signature': signature,
                    'timestamp': timestamp,
                    'startCreateDateTime': startCreateDateTime,
                    'endCreateDateTime': endCreateDateTime
                }

                response_pend = requests.request("GET", url, headers=headers, data=payload, verify=False)
                responseObj_pend = json.loads(response_pend.text.replace('\\r\\n', ''))

            with open(user_folder_path+"\\Documents\\API\\OUTPUT\\pend_output_"+Path(file).stem+".json", 'w') as outfile:
                    #json.dump(responseObj_pend, outfile, indent=4)
                    #requests_tCode = str(responseObj_pend)
                    #requests_tCode = requests_tCode.replace('\\r\\n', '')
                    json.dump(responseObj_pend, outfile, indent=4)
                    outfile.close()
                    print("pending transaction done")

                
print(responseObj_pend)

# encoding pendingTranscations to csv for next step -- uploadPayPorder
Jar_cmd_convert = os.popen('java -jar '+ user_folder_path+'\\Documents\\API\\Converter.jar '+ user_folder_path+'\\Documents\\API\\INPUT\\uploadPayPorder\\files\\pendingTransactioncsv.csv ' + responseObj_pend["pendingTransactions"]).read()