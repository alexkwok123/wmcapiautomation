import glob
import json
import os
from pathlib import Path

import requests

user_profile = os.environ['USERPROFILE']
user_folder_path = user_profile.replace('\\','\\')

path, dirs, files = next(os.walk(user_folder_path+'\\Documents\\API\\INPUT\\\sameCompanySwitch\\body\\'))
file_count_sameComSwitchBody = len(files)

Jar_cmd = os.popen('java -jar '+ user_folder_path+'\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/accounts/sameCompanySwitching').read()
Default = open(user_folder_path+"\\Documents\\API\\INPUT\\default_sameCompanySwitch.json", 'w')
Default.write('{\n'+Jar_cmd+'}')
Default.close()

with open(user_folder_path+"\\Documents\\API\\INPUT\\default_sameCompanySwitch.json", 'r') as outfile_sub:
    json_de_sameComSwitch = outfile_sub.read()
    requests_de_sameComSwitch = json.loads(json_de_sameComSwitch)
    #print(requests_de)

if file_count_sameComSwitchBody == 0:
    print("there is missing body")
else:
    for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\sameCompanySwitch\\body\\*.json'):
        with open(file, 'r') as f:
            sameComSwitch_body = json.load(f)
            sameComSwitch_body = str(sameComSwitch_body)
            sameComSwitch_body = sameComSwitch_body.replace('\'', '\"')
            requests_body_sameComSwitch = json.loads(sameComSwitch_body)
            Path(file).stem
            # for i in range(len(bodyData)):
            # temp = tmp.replace("'", '"')
            # tweets[0]["accountid"]
            # temp = json.dumps(tmp, indent=4)

            url = "https://localhost:8181" + requests_de_sameComSwitch["url"]
            apikey = requests_de_sameComSwitch["apikey"]
            signature = requests_de_sameComSwitch["signature"]
            timestamp = requests_de_sameComSwitch["timestamp"]

            smaeComSwithc_body = sameComSwitch_body

            payload = smaeComSwithc_body

            headers = {
                'apikey': apikey,
                'signature': signature,
                'timestamp': timestamp,
                'Content-Type': "application/json"
            }

            response_sameComSwitch = requests.request("POST", url, headers=headers, data=payload, verify=False)
            responseObj_sameComSwitch = json.loads(response_sameComSwitch.text)

            with open(user_folder_path + "\\Documents\\API\\OUTPUT\\sameComSwitch_output_" + Path(file).stem + ".json",
                        'w') as outfile:
                json.dump(responseObj_sameComSwitch, outfile, indent=4)
                outfile.close()
                print(responseObj_sameComSwitch)
print("Same Company Switch is completed")