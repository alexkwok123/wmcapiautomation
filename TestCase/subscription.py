import glob
import json
import os
from pathlib import Path

import requests

user_profile = os.environ['USERPROFILE']
user_folder_path = user_profile.replace('\\','\\')

Jar_cmd = os.popen('java -jar '+ user_folder_path+'\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/accounts/subscription').read()
Default = open(user_folder_path+"\\Documents\\API\\INPUT\\default_subscription.json", 'w')
Default.write('{\n'+Jar_cmd+'}')
Default.close()

with open(user_folder_path+"\\Documents\\API\\INPUT\\default_subscription.json", 'r') as outfile_sub:
    json_de_sub = outfile_sub.read()
    requests_de = json.loads(json_de_sub)
    #print(requests_de)

for file in glob.glob(user_folder_path+'\\Documents\\API\\INPUT\\\subscription\\body\\*.json'):
    with open(file, 'r') as f:
        sub_body = json.load(f)
        sub_body = str(sub_body)
        sub_body = sub_body.replace('\'','\"')
        Path(file).stem
        #tmp = tmp.replace('[', '')
        #bodyData = tmp.replace('}', '},')
        # for i in range(len(bodyData)):
        # temp = tmp.replace("'", '"')
        # tweets[0]["accountid"]
        # temp = json.dumps(tmp, indent=4)
        url = "https://localhost:8181"+requests_de["url"]

        fileHeader = open(user_folder_path+'\\Documents\\API\\INPUT\\subscription\\header\\headers.json', 'r')
#fileBody = open('C:\\Users\\alex.kwok\\Documents\\API\\INPUT\\\subscription\\body\\body1.json', 'r')

        json_header = fileHeader.read()
#json_body = fileBody.read()
#print(json_body)

        requests_header = json.loads(json_header)

        apikey = requests_de["apikey"]
        signature = requests_de["signature"]
        timestamp = requests_de["timestamp"]
        contentType = requests_header["Content-Type"]
        #cookie = requests_header["Cookie"]

        payload = sub_body

        headers = {
            'apikey': apikey,
            'signature': signature,
            'timestamp': timestamp,
            'Content-Type': contentType
            #'Cookie': cookie
        }

        response = requests.request("POST", url, headers=headers, data=payload, verify=False)
        responseObj = json.loads(response.text)
        #print(responseObj)


        with open(user_folder_path+"\\Documents\\API\\OUTPUT\\sub_output_"+Path(file).stem+".json", 'w') as outfile:
            json.dump(responseObj, outfile, indent=4)
            outfile.close()
            print("done")


        # fileOutput = open("C:\\Users\\alex.kwok\\PycharmProjects\\WMCAPIAutomation\\TestCase\\output.json", 'r')
        # json_output = fileOutput.read()
        # requests_output = json.loads(json_output)
        # # print(requests_output)
        #
        # fileExResult = open('C:\\Users\\alex.kwok\\Documents\\API\\EXPECTED\\Result1.json', 'r')
        # json_result = fileExResult.read()
        # requests_result = json.loads(json_result)
        # # print(requests_result)
        #
        # # diff = jsondiff.diff(requests_output, requests_result)
        # diff = jsondiff.diff(requests_result, requests_output)
        #
        # if diff:
        #     diff_report = open("C:\\Users\\alex.kwok\\Documents\\API\\DIFF\\diff.txt", 'w')
        #     diff_report.write("Here is/are the differences\n" + str(diff) + '\n')
        #     diff_report.close
        #     print("Here is/are the differences\n" + str(diff) + '\n')
        # else:
        #     results = open("C:\\Users\\alex.kwok\\Documents\\API\\DIFF\\results.txt", 'w')
        #     results.write(str(requests_result))
        #     print("pass")


