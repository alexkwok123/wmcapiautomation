import glob
import json
import os
from pathlib import Path
import requests

user_profile = os.environ['USERPROFILE']
user_folder_path = user_profile.replace('\\', '\\')

Jar_cmd = os.popen(
    'java -jar ' + user_folder_path + '\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/uploadPayPOrder').read()
Default = open(user_folder_path + "\\Documents\\API\\INPUT\\default_uploadPayPorder.json", 'w')
Default.write('{\n' + Jar_cmd + '}')
Default.close()

with open(user_folder_path + "\\Documents\\API\\INPUT\\default_uploadPayPorder.json", 'r') as outfile_payporder:
    json_de_payporder = outfile_payporder.read()
    requests_de_payporder = json.loads(json_de_payporder)

path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\body\\'))
file_count_payporderbody = len(files)
path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\files\\'))
file_count_payporderfiles = len(files)

if file_count_payporderbody == 0:
    print('there is missing PayPOrder body')
else:
    for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\body\\*.json'):
        with open(file, 'r') as f:
            payporder_body = json.load(f)
            payporder_body = str(payporder_body)
            payporder_body = payporder_body.replace('\'', '\"')
            requests_body_payporder = json.loads(payporder_body)
            if file_count_payporderfiles != 0:
                files = {'pendingTransactions': ('pendingTransactions', open(
                    user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\files\\pendingTransactioncsv.csv',
                    'rb'))}
                url = "https://localhost:8181" + requests_de_payporder["url"]
                apikey = requests_de_payporder["apikey"]
                signature = requests_de_payporder["signature"]
                timestamp = requests_de_payporder["timestamp"]
                userId = requests_body_payporder["userId"]
                #contentType = requests_body_payporder["Content-Type"]

                payporder_body = {
                    'userId': userId
                }

                payload = payporder_body

                headers = {
                    'apikey': apikey,
                    'signature': signature,
                    'timestamp': timestamp
                    # 'userId': userId,
                    # 'Content-Type': contentType
                }
                # print(pendingTransactionFile)
                response_payporder = requests.request("POST", url, headers=headers, data=payload, files=files,
                                                      verify=False)
                responseObj_payporder = json.loads(response_payporder.text)

            with open(user_folder_path + "\\Documents\\API\\OUTPUT\\payporder_output_" + Path(file).stem + ".json",
                      'w') as outfile:
                json.dump(responseObj_payporder, outfile, indent=4)
                outfile.close()
            with open("uploadReferenceNo.txt", 'w') as uploadReferenceNo:
                json.dump(responseObj_payporder, uploadReferenceNo, indent=4)
                uploadReferenceNo.close()
                print("uploadPayPorder done")
