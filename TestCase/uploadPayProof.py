import glob
import json
import os
from pathlib import Path


import requests

user_profile = os.environ['USERPROFILE']
user_folder_path = user_profile.replace('\\','\\')

Jar_cmd = os.popen('java -jar '+ user_folder_path+'\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/uploadPayProof').read()
Default = open(user_folder_path+"\\Documents\\API\\INPUT\\default_uploadPayProof.json", 'w')
Default.write('{\n'+Jar_cmd+'}')
Default.close()

with open(user_folder_path + "\\Documents\\API\\INPUT\\default_uploadPayProof.json", 'r') as outfile_PayProof:
    json_de_PayProof = outfile_PayProof.read()
    requests_de_PayProof = json.loads(json_de_PayProof)

with open("uploadReferenceNo.txt", 'r') as outfile_uploadReferenceNo:
    json_uploadReferenceNo = outfile_uploadReferenceNo.read()
    requests_uploadReferenceNo = json.loads(json_uploadReferenceNo)

path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\body\\'))
file_count_PayProofbody = len(files)
path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\PDF\\'))
file_count_PayProoffiles = len(files)

if file_count_PayProofbody == 0:
    print("there is missing PayProof body")
else:
    for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\body\\*.json'):
        with open(file, 'r') as f:
            payproof_body = json.load(f)
            payproof_body = str(payproof_body)
            payproof_body = payproof_body.replace('\'', '\"')
            requests_body_payproof = json.loads(payproof_body)
            if file_count_PayProoffiles != 0:
                folder = user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\PDF\\'
                pdf = glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\PDF\\*')
                x: int = file_count_PayProoffiles
                upload_list = []
                for files in os.listdir(folder):
                    with open("{folder}{name}".format(folder=folder, name=files), "rb") as data:
                        upload_list.append(files)
                count = 0
                pdf_source = []
                while count < x:
                    files = 'paymentProof', (open(
                    folder+upload_list[count],
                    'rb'))
                    count +=1
                    pdf_source.append(files)
                url = "https://localhost:8181" + requests_de_PayProof["url"]
                apikey = requests_de_PayProof["apikey"]
                signature = requests_de_PayProof["signature"]
                timestamp = requests_de_PayProof["timestamp"]
                userId = requests_body_payproof["userId"]
                fileCount = requests_body_payproof["fileCount"]
                paymentComplete = requests_body_payproof["paymentComplete"]
                uploadReferenceNo = requests_uploadReferenceNo["uploadReferenceNo"]

                files = pdf_source

                payproof_body = {
                    'userId': userId,
                    'fileCount': fileCount,
                    'paymentComplete': paymentComplete,
                    'uploadReferenceNo': uploadReferenceNo
                }

                payload = payproof_body

                headers = {
                    'apikey': apikey,
                    'signature': signature,
                    'timestamp': timestamp
                }
                # print(pendingTransactionFile)
                response_payproof = requests.request("POST", url, headers=headers, data=payload, files=files,
                                                     verify=False)
                print(response_payproof.text)
                responseObj_payproof = json.loads(response_payproof.text)

                with open(user_folder_path + "\\Documents\\API\\OUTPUT\\payproof_output_" + Path(
                        file).stem + ".json",
                          'w') as outfile:
                    json.dump(responseObj_payproof, outfile, indent=4)
                    outfile.close()
