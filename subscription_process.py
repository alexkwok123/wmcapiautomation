import glob
import json
import os
from pathlib import Path
import requests

#version 1.1

user_profile = os.environ['USERPROFILE']
user_folder_path = user_profile.replace('\\', '\\')
contentType = 'application/json'

Jar_cmd = os.popen(
    'java -jar ' + user_folder_path + '\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/accounts/subscription').read()
Default = open(user_folder_path + "\\Documents\\API\\INPUT\\default_subscription.json", 'w')
Default.write('{\n' + Jar_cmd + '}')
Default.close()

with open(user_folder_path + "\\Documents\\API\\INPUT\\default_subscription.json", 'r') as outfile_sub:
    json_de_sub = outfile_sub.read()
    requests_de = json.loads(json_de_sub)
    # print(requests_de)

for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\\subscription\\body\\*.json'):
    with open(file, 'r') as f:
        sub_body = json.load(f)
        sub_body = str(sub_body)
        sub_body = sub_body.replace('\'', '\"')
        Path(file).stem
        # tmp = tmp.replace('[', '')
        # bodyData = tmp.replace('}', '},')
        # for i in range(len(bodyData)):
        # temp = tmp.replace("'", '"')
        # tweets[0]["accountid"]
        # temp = json.dumps(tmp, indent=4)
        url = "https://windowapi:8181" + requests_de["url"]

        fileHeader = open(user_folder_path + '\\Documents\\API\\INPUT\\subscription\\header\\headers.json', 'r')
        # fileBody = open('C:\\Users\\alex.kwok\\Documents\\API\\INPUT\\\subscription\\body\\body1.json', 'r')

        json_header = fileHeader.read()
        # json_body = fileBody.read()
        # print(json_body)

        requests_header = json.loads(json_header)

        apikey = requests_de["apikey"]
        signature = requests_de["signature"]
        timestamp = requests_de["timestamp"]
        contentType = 'application/json'

        payload = sub_body

        headers = {
            'apikey': apikey,
            'signature': signature,
            'timestamp': timestamp,
            'Content-Type': contentType
        }

        response = requests.request("POST", url, headers=headers, data=payload, verify=False)
        # print(response)
        responseObj = json.loads(response.text)
        # print(responseObj)

        with open(user_folder_path + "\\Documents\\API\\OUTPUT\\sub_output_" + Path(file).stem + ".json",
                  'w') as outfile:
            json.dump(responseObj, outfile, indent=4)
            outfile.close()
            print("done")

Jar_cmd = os.popen(
    'java -jar ' + user_folder_path + '\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/accounts/pendingTransaction/csv').read()
Default = open(user_folder_path + "\\Documents\\API\\INPUT\\default_pendingTrans.json", 'w')
Default.write('{\n' + Jar_cmd + '}')
Default.close()

with open(user_folder_path + "\\Documents\\API\\INPUT\\default_pendingTrans.json", 'r') as outfile_pend:
    json_de_pend = outfile_pend.read()
    requests_de_pend = json.loads(json_de_pend)

path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\\pendingTrans\\body\\'))
file_count_pendbody = len(files)
path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\\pendingTrans\\header\\'))
file_count_pendheader = len(files)
if file_count_pendheader == 0:
    print("there is missing header")
else:
    for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\\pendingTrans\\header\\*.json'):
        with open(file, 'r') as f:
            pend_header = json.load(f)
            pend_header = str(pend_header)
            pend_header = pend_header.replace('\'', '\"')
            requests_header_pend = json.loads(pend_header)
            Path(file).stem
            # for i in range(len(bodyData)):
            # temp = tmp.replace("'", '"')
            # tweets[0]["accountid"]
            # temp = json.dumps(tmp, indent=4)

            if file_count_pendbody != 0:
                for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\\pendingTrans\\header\\*.json'):
                    with open(file, 'r') as f:
                        pend_body = json.load(f)
                        pend_body = str(pend_body)
                        pend_body = pend_body.replace('\'', '\"')
                        requests_body_pend = json.loads(pend_body)
            else:
                url = "https://windowapi:8181" + requests_de_pend["url"]
                apikey = requests_de_pend["apikey"]
                signature = requests_de_pend["signature"]
                timestamp = requests_de_pend["timestamp"]
                startCreateDateTime = requests_header_pend["startCreateDateTime"]
                endCreateDateTime = requests_header_pend["endCreateDateTime"]
                pend_body = {}

                payload = pend_body

                headers = {
                    'apikey': apikey,
                    'signature': signature,
                    'timestamp': timestamp,
                    'startCreateDateTime': startCreateDateTime,
                    'endCreateDateTime': endCreateDateTime
                }

                response_pend = requests.request("GET", url, headers=headers, data=payload, verify=False)
                responseObj_pend = json.loads(response_pend.text.replace('\\r\\n', ''))

            with open(user_folder_path + "\\Documents\\API\\OUTPUT\\pend_output_" + Path(file).stem + ".json",
                      'w') as outfile:
                json.dump(responseObj_pend, outfile, indent=4)
                outfile.close()

                print("pending transaction done")

print(responseObj_pend["pendingTransactions"])

# encoding pendingTranscations to csv for next step -- uploadPayPorder
Jar_cmd_convert = os.popen(
    'java -jar ' + user_folder_path + '\\Documents\\API\\Converter.jar ' + user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\files\\pendingTransactioncsv.csv ' +
    responseObj_pend["pendingTransactions"]).read()

Jar_cmd = os.popen(
    'java -jar ' + user_folder_path + '\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/uploadPayPOrder').read()
Default = open(user_folder_path + "\\Documents\\API\\INPUT\\default_uploadPayPorder.json", 'w')
Default.write('{\n' + Jar_cmd + '}')
Default.close()

with open(user_folder_path + "\\Documents\\API\\INPUT\\default_uploadPayPorder.json", 'r') as outfile_payporder:
    json_de_payporder = outfile_payporder.read()
    requests_de_payporder = json.loads(json_de_payporder)

path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\body\\'))
file_count_payporderbody = len(files)
path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\files\\'))
file_count_payporderfiles = len(files)

if file_count_payporderbody == 0:
    print('there is missing PayPOrder body')
else:
    for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\body\\*.json'):
        with open(file, 'r') as f:
            payporder_body = json.load(f)
            payporder_body = str(payporder_body)
            payporder_body = payporder_body.replace('\'', '\"')
            requests_body_payporder = json.loads(payporder_body)
            if file_count_payporderfiles != 0:
                files = {'pendingTransactions': ('pendingTransactions', open(
                    user_folder_path + '\\Documents\\API\\INPUT\\uploadPayPorder\\files\\pendingTransactioncsv.csv',
                    'rb'))}
                url = "https://windowapi:8181" + requests_de_payporder["url"]
                apikey = requests_de_payporder["apikey"]
                signature = requests_de_payporder["signature"]
                timestamp = requests_de_payporder["timestamp"]
                userId = requests_body_payporder["userId"]
                #contentType = requests_body_payporder["Content-Type"]

                payporder_body = {
                    'userId': userId
                }

                payload = payporder_body

                headers = {
                    'apikey': apikey,
                    'signature': signature,
                    'timestamp': timestamp
                    # 'userId': userId,
                    # 'Content-Type': contentType
                }
                # print(pendingTransactionFile)
                response_payporder = requests.request("POST", url, headers=headers, data=payload, files=files,
                                                      verify=False)
                responseObj_payporder = json.loads(response_payporder.text)

            with open(user_folder_path + "\\Documents\\API\\OUTPUT\\payporder_output_" + Path(file).stem + ".json",
                      'w') as outfile:
                json.dump(responseObj_payporder, outfile, indent=4)
                outfile.close()
            with open("uploadReferenceNo.txt", 'w') as uploadReferenceNo:
                json.dump(responseObj_payporder, uploadReferenceNo, indent=4)
                uploadReferenceNo.close()
                print("uploadPayPorder done")

Jar_cmd = os.popen(
    'java -jar ' + user_folder_path + '\\Documents\\API\\API_http.jar 9121e2f3-14f4-404d-8216-1735a2b6a2e4 /PWS/restfulapi/uploadPayProof').read()
Default = open(user_folder_path + "\\Documents\\API\\INPUT\\default_uploadPayProof.json", 'w')
Default.write('{\n' + Jar_cmd + '}')
Default.close()

with open(user_folder_path + "\\Documents\\API\\INPUT\\default_uploadPayProof.json", 'r') as outfile_PayProof:
    json_de_PayProof = outfile_PayProof.read()
    requests_de_PayProof = json.loads(json_de_PayProof)

with open("uploadReferenceNo.txt", 'r') as outfile_uploadReferenceNo:
    json_uploadReferenceNo = outfile_uploadReferenceNo.read()
    requests_uploadReferenceNo = json.loads(json_uploadReferenceNo)

path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\body\\'))
file_count_PayProofbody = len(files)
path, dirs, files = next(os.walk(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\PDF\\'))
file_count_PayProoffiles = len(files)

if file_count_PayProofbody == 0:
    print("there is missing PayProof body")
else:
    for file in glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\body\\*.json'):
        with open(file, 'r') as f:
            payproof_body = json.load(f)
            payproof_body = str(payproof_body)
            payproof_body = payproof_body.replace('\'', '\"')
            requests_body_payproof = json.loads(payproof_body)
            if file_count_PayProoffiles != 0:
                folder = user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\PDF\\'
                pdf = glob.glob(user_folder_path + '\\Documents\\API\\INPUT\\uploadPayProof\\PDF\\*')
                x: int = file_count_PayProoffiles
                upload_list = []
                for files in os.listdir(folder):
                    with open("{folder}{name}".format(folder=folder, name=files), "rb") as data:
                        upload_list.append(files)
                count = 0
                pdf_source = []
                while count < x:
                    files = 'paymentProof', (open(
                    folder+upload_list[count],
                    'rb'))
                    count +=1
                    pdf_source.append(files)
                url = "https://windowapi:8181" + requests_de_PayProof["url"]
                apikey = requests_de_PayProof["apikey"]
                signature = requests_de_PayProof["signature"]
                timestamp = requests_de_PayProof["timestamp"]
                userId = requests_body_payproof["userId"]
                fileCount = requests_body_payproof["fileCount"]
                paymentComplete = requests_body_payproof["paymentComplete"]
                uploadReferenceNo = requests_uploadReferenceNo["uploadReferenceNo"]

                files = pdf_source
                
                payproof_body = {
                    'userId': userId,
                    'fileCount': fileCount,
                    'paymentComplete': paymentComplete,
                    'uploadReferenceNo': uploadReferenceNo
                }

                payload = payproof_body

                headers = {
                    'apikey': apikey,
                    'signature': signature,
                    'timestamp': timestamp
                }
                # print(pendingTransactionFile)
                response_payproof = requests.request("POST", url, headers=headers, data=payload, files=files,
                                                     verify=False)
                print(response_payproof.text)
                responseObj_payproof = json.loads(response_payproof.text)

                with open(user_folder_path + "\\Documents\\API\\OUTPUT\\payproof_output_" + Path(
                        file).stem + ".json",
                          'w') as outfile:
                    json.dump(responseObj_payproof, outfile, indent=4)
                    outfile.close()
